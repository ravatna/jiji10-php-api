<?php

/**
 * Generic class for control member data.
 */
class RedeemItem extends ADataModel{
    /**
     * Identity of data
     * @param integer
     */
    public $id; 
    
    /**
     * Type of card
     * @param string
     */
    public $name; 

    /**
     * Description of card
     * @param string
     */
    public $desc;

     /**
     * Created at datetime
     */
    public $image;


    /**
     * Bday of member
     * @param DateTime
     */
    public $expireDate;

    /**
     * Start Active date
     * @param DateTime
     */
    public $startDate;

    
} // .End class Member

class RedeemItemManage extends DbManagement {
    function __construct(){
        parent::__construct("localhost","root","!@#$%^123456","jijicrm");
        $this->table_name = "redeem_item";
        $this->pk_id = "id";        
        $this->defaultColumns = array( "id", "name", "image","score","expire_date","start_date","created_at","updated_at","created_by","updated_by");
    }

}