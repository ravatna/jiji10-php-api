<?php


/**
 * Generic class for control member data.
 */
class Member  extends ADataModel{

    /**
     * PK of data
     */
    public $id;

    /**
     * Name of member
     * @param string
     */
    public $name; 
    /**
     * Surname of member
     * @param string
     */
    public $surname;

    /**
     * E-Mail of member
     * @param string
     */
    public $email;
    /**
     * Mobile of member
     * @param string
     */
    public $mobile;
    /**
     * Address of member
     * @param string
     */
    public $addr;

    /**
     * Member gender
     * @param string 
     */
    public $sex;

    /**
     * Age of member
     * @param integer
     */
    public $age;

    /**
     * Bday of member
     * @param DateTime
     */
    public $birthday;

     /**
     * Created at datetime
     */
    public $createdAt;

    /**
     * Updated at datetime
     */
    public $updatedAt;

    /**
     * Created by username
     */
    public $createdBy;

    /**
     * Updated by username
     */
    public $updatedBy;

    /**
     * Card of member to handles
     * @param CardMemberManage 
     */
    public $cardMember;

} // .End class Member


class MemberManage extends DbManagement {
    
    function __construct(){
        
        parent::__construct("localhost","root","Clexpert","crm_neo");
        $this->table_name = "member";
        $this->pk_id = "id";

        $this->where = array();
        $this->columns = array();
        $this->defaultColumns = array( "id","fname", "lname", "email", "phone", "uname", "pword", "bday","id_card", "age", "addr", "city", "district", "province", "zipcode", "is_lock", "created_at", "created_by", "updated_at", "updated_by");
    } // .End


    function findByUnameAndPword($uname,$pword){
        $this->columns = $this->defaultColumns;
        
        $this->wheres = array("uname"=> $uname,"pword"=>$pword);
        $result = $this->select();
        $my_data = array();
        while($a = $this->Db->fetchArray()){
            $aa = array();    
            foreach($a as $k => $v){
                $aa[$k] = $v;
            }
            $my_data[] = $aa;
        } // .End while
        return $my_data;
    }


    function findByMobile($m){
        $this->columns = $this->defaultColumns;
        
        $this->wheres = array("phone"=> $m);
        $result = $this->select();
        $my_data = array();
        while($a = $this->Db->fetchArray()){
            $aa = array();    
            foreach($a as $k => $v){
                $aa[$k] = $v;
            }
            $my_data[] = $aa;
        } // .End while
        return $my_data;
    }

    
    
    function findByMobileAndCardNo($m,$c){
        $this->columns = $this->defaultColumns;
        
        $this->wheres = array("phone"=> $m, "uname"=>$c);
        $result = $this->select();
        $my_data = array();
        while($a = $this->Db->fetchArray()){
            $aa = array();    
            foreach($a as $k => $v){
                $aa[$k] = $v;
            }
            $my_data[] = $aa;
        } // .End while
        return $my_data;
    }
}

