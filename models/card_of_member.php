<?php


class CardOfMemberManage extends DbManagement {
    
    function __construct(){
        
        parent::__construct("localhost","root","Clexpert","crm_neo");
        $this->table_name = "card_of_member";
        $this->pk_id = "id";

        $this->where = array();
        $this->columns = array();
        $this->defaultColumns = array( "id","member_id", "card_no", "created_at", "created_by", "updated_at", "updated_by");
    } // .End

    /**
     * @param $m card number
     */
    function findByCardNo($card_no){
        $this->columns = $this->defaultColumns;
        
        $this->wheres = array("card_no"=> $card_no);
        $result = $this->select();
        $my_data = array();
        while($a = $this->Db->fetchArray()){
            $aa = array();    
            foreach($a as $k => $v){
                $aa[$k] = $v;
            }
            $my_data[] = $aa;
        } // .End while
        return $my_data;
    }// .End findByCardNo()


}

