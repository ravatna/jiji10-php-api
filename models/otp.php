<?php

/**
 * Generic class for control member data.
 */
class Otp extends ADataModel{
    /**
     * Identity of data
     * @param integer
     */
    public $id; 
    
    /**
     * reference number for match otp
     * @param string
     */
    public $ref_no; 

    /**
     * one time password
     * @param string
     */
    public $otp;


    /**
     * start valid datetime
     */
    public $start_date;

    /**
     * end valid datetime
     */
    public $end_date;


     /**
     * Created at datetime
     */
    public $createdAt;

    /**
     * Updated at datetime
     */
    public $updatedAt;

    /**
     * Created by username
     */
    public $createdBy;

    /**
     * Updated by username
     */
    public $updatedBy;
    
} // .End class Member

class OtpManage extends DbManagement {
    function __construct(){
        parent::__construct("localhost","root","Clexpert","crm_neo");
        $this->table_name = "otp";
        $this->pk_id = "id";

        $this->where = array();
        $this->columns = array();
        $this->defaultColumns = array( "id","ref_no", "otp", "start_date", "end_date", "status", "created_at", "created_by", "updated_at", "updated_by");
    } // .End

    
    /**
     * 
     * @param $mobile
     * @param $ref_no
     * @param $opt
     */
    function findByMobileAndRefNoAndOTP($ref_no,$otp){
        $this->columns = $this->defaultColumns;
        
        $this->wheres = array("ref_no"=> $ref_no,"otp"=>$otp,"status"=>"wait");
        $result = $this->select();
        $my_data = array();
        while($a = $this->Db->fetchArray()){
            $aa = array();    
            foreach($a as $k => $v){
                $aa[$k] = $v;
            }
            $my_data[] = $aa;
        } // .End while
        return $my_data;
    }



    /**
     * Check duplication by column target
     */
    function isValidOTP($ref_no,$otp){
        
        $this->columns = $this->defaultColumns;
        $this->wheres = array("ref_no"=> $ref_no, "otp"=>$otp, "status"=>"wait");
        
        $result = $this->select();
        
        $b = false;
        if($result->num_rows > 0) {
            $b = true;
        }
        return $b;
    } // .End isDuplicate
}