<?php

/**
 * Generic class for control member data.
 */
class Card extends ADataModel{
    /**
     * Identity of data
     * @param integer
     */
    public $id; 
    
   

    /**
     * Type of card
     * @param string
     */
    public $type; 

    /**
     * Description of card
     * @param string
     */
    public $desc;

     /**
     * Created at datetime
     */
    public $createdAt;

    /**
     * Updated at datetime
     */
    public $updatedAt;

    /**
     * Created by username
     */
    public $createdBy;

    /**
     * Updated by username
     */
    public $updatedBy;
    
} // .End class Member

class CardManage extends DbManagement {
    function __construct(){
        parent::__construct("localhost","root","!@#$%^123456","jijicrm");
        $this->table_name = "card";
        $this->pk_id = "id";    
        
        $this->defaultColumns = array("id","type","desc");
    }

    
}