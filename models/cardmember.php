<?php

/**
 * Generic class for control member data.
 */
class CardMember {
    /**
     * Identity of data
     * @param integer
     */
    public $id; 


    /**
     * FK card
     * @param integer
     */
    public $card_id; 

    /**
     * Card Number
     * @param string
     */
    public $cardNo;
    
    /**
     * Bday of member
     * @param DateTime
     */
    public $expireDate;

    /**
     * Start Active date
     * @param DateTime
     */
    public $startDate;

     /**
     * Created at datetime
     */
    public $createdAt;

    /**
     * Updated at datetime
     */
    public $updatedAt;

    /**
     * Created by username
     */
    public $createdBy;

    /**
     * Updated by username
     */
    public $updatedBy;

    /**
     * Object to collect data
     * @param MemberManage
     */
    public $memberManage;
    
} // .End class Member

class CardMemberManage extends DbManagement {

    /**
     * @param $memberManage MemberManage
     */
    public $memberManage;

    function __construct(){
        parent::__construct("localhost","root","Clexpert","crm_neo");
        $this->table_name = "card_member";
        $this->pk_id = "id";

        $this->defaultColumns = array( 
            "CardNumber", "IdentifierNumber", "TelephoneNumber", "CardType", "StartDate", 
            "ExpireDate", "PercentDiscount", "CardValue", "Point", "CashCoupon", "TitleName", 
            "GivenName", "FamilyName", "CustomerID", "Birthdate", "Active", "Result", 
            "Message","MarryDate","ApplyMarryDateGift","ApplyBirthdateGift");
        $this->memberManage = new MemberManage();
    }

    function findMemberById($member_id){
        $this->memberManage->columns = array( "id","fname", "lname", "email", "phone", "uname", "pword", "bday", "age", "addr", "city", "district", "province", "zipcode", "is_lock", "created_at", "created_by", "updated_at", "updated_by");
        $this->memberManage->wheres = array("id"=> $member_id);

        $result = $this->memberManage->select();
        $my_data = array();
        while($a = $this->memberManage->Db->fetchArray()){
            $aa = array();
            
            foreach($a as $k => $v){
                $aa[$k] = $v;
            }
            
            $my_data[] = $aa;
        } // .End while
        
        return $my_data;
    }

    function findCardByTelephone($no){
        $this->columns = $this->defaultColumns;
        $this->wheres = array("TelephoneNumber"=> $no);

        $result = $this->select();
        $my_data = array();
        while($a = $this->Db->fetchArray()){
            $aa = array();
            
            foreach($a as $k => $v){
                $aa[$k] = $v;
            }
            
            $my_data[] = $aa;
        } // .End while
        
        return $my_data;
    }

    function findCardByMemberId($id){
        $this->columns = $this->defaultColumns;
        $this->wheres = array("member_id"=> $id);

        $result = $this->select();
        $my_data = array();
        while($a = $this->Db->fetchArray()){
            $aa = array();
            
            foreach($a as $k => $v){
                $aa[$k] = $v;
            }
            
            $my_data[] = $aa;
        } // .End while
        
        return $my_data;
    }
    

    // /**
    //  * Add new data
    //  */
    // function add($columns = array(),$values = array()){
    //     if(count($columns) > 0){
    //         $this->columns = $columns;
    //         $this->values = $values;
    //     }
    //     $result = $this->insert();
    //     return $result->num_rows;
    // }

    // function save($id,$columns = array(),$values = array()){
    //     if(count($columns) > 0){
    //         $this->columns = $columns;
    //         $this->values = $values;
    //         $this->wheres = array("id" =>$id);
    //     }
  
    //     $result = $this->update();
    //     return $result->num_rows;
    // }
    
}