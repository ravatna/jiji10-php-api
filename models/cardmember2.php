<?php

/**
 * Generic class for control member data.
 */
class CardMember2 {
    /**
     * Identity of data
     * @param integer
     */
    public $id; 


    /**
     * FK card
     * @param integer
     */
    public $card_id; 

    /**
     * Card Number
     * @param string
     */
    public $cardNo;
    
    /**
     * Bday of member
     * @param DateTime
     */
    public $expireDate;

    /**
     * Start Active date
     * @param DateTime
     */
    public $startDate;

     /**
     * Created at datetime
     */
    public $createdAt;

    /**
     * Updated at datetime
     */
    public $updatedAt;

    /**
     * Created by username
     */
    public $createdBy;

    /**
     * Updated by username
     */
    public $updatedBy;

    /**
     * Object to collect data
     * @param MemberManage
     */
    public $memberManage;
    
} // .End class Member

class CardMemberManage2 extends DbManagement {

    /**
     * @param $memberManage MemberManage
     */
    public $memberManage;

    function __construct(){
        parent::__construct("localhost","root","!@#$%^123456","jijicrm");
        $this->table_name = "member_card";
        $this->pk_id = "id";
        $this->defaultColumns = array( "id","card_id","card_no","expire_date","start_date","created_at","updated_at","created_by","updated_by");
        $this->memberManage = new MemberManage();
    }

    function findAll($columns = array()){
        if(count($columns) > 0){
            $this->columns = $columns;
        }else{
            $this->columns = $this->defaultColumns;
        }
        
        $this->wheres = array();
        
        $result = $this->select();
        $my_data = array();
        while($a = $this->Db->fetchArray()){
            $aa = array();
            
            foreach($a as $k => $v){
                $aa[$k] = $v;
            }
            
            $my_data[] = $aa;
        } // .End while
        
        return $my_data;
    }

    function findById($id){
        
        $this->columns = array( "id","card_id","card_no","expire_date","start_date","created_at","updated_at","created_by","updated_by");
        $this->wheres = array("id"=> $id);
        
        $result = $this->select();
        $my_data = array();
        while($a = $this->Db->fetchArray()){
            $aa = array();
            
            foreach($a as $k => $v){
                $aa[$k] = $v;
            }
            
            $my_data[] = $aa;
        } // .End while
        
        return $my_data;
    }

    function findByMemberId($member_id){
        $this->memberManage->columns = array( "id","fname", "lname", "email", "phone", "uname", "pword", "bday", "age", "addr", "city", "district", "province", "zipcode", "is_lock", "created_at", "created_by", "updated_at", "updated_by");
        $this->memberManage->wheres = array("id"=> $member_id);

        $result = $this->memberManage->select();
        $my_data = array();
        while($a = $this->memberManage->Db->fetchArray()){
            $aa = array();
            
            foreach($a as $k => $v){
                $aa[$k] = $v;
            }
            
            $my_data[] = $aa;
        } // .End while
        
        return $my_data;
    }
}