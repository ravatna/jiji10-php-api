<?php
/**
 * Extends class from DateTime 
 * 
 * Instantiate a DateTime with microseconds.
 * $d = new DateTime('2011-01-01T15:03:01.012345Z');
 * Output the microseconds.
 * echo $d->format('u'); // 012345

 * Output the date with microseconds.
 * echo $d->format('Y-m-d\TH:i:s.u'); // 2011-01-01T15:03:01.012345
 
 * Usage:

 * <?php
 * $birthday = new JIJIDateTime('1879-03-14');

 * // Example 1
 * echo $birthday;
 * // Result: 1879-03-14 00:00

 * // Example 2
 * echo '<p>Albert Einstein would now be ', $birthday->getAge(), ' years old.</p>';
 * // Result: <p>Albert Einstein would now be 130 years old.</p>

 * // Example 3
 * echo '<p>Albert Einstein would now be ', $birthday->diff()->format('%y Years, %m Months, %d Days'), ' old.</p>';
 * // Result: <p>Albert Einstein would now be 130 Years, 10 Months, 10 Days old.</p>

 * // Example 4
 * echo '<p>Albert Einstein was on 2010-10-10 ', $birthday->getAge('2010-10-10'), ' years old.</p>';
 * // Result: <p>Albert Einstein was on 2010-10-10 131 years old.</p>
 * ?>
 */



class JIJIDateTime extends DateTime {

    /**
     * Return Date in ISO8601 format
     *
     * @return String
     */
    public function __toString() {
        return $this->format('Y-m-d H:i');
    }

    /**
     * Return difference between $this and $now
     *
     * @param Datetime|String $now
     * @return DateInterval
     */
    public function is_diff($now = 'NOW') {
        if(!($now instanceOf DateTime)) {
            $now = new DateTime($now);
        }

        return parent::diff($now);
    }

    /**
     * Return Age in Years
     *
     * @param Datetime|String $now
     * @return Integer
     */
    public function getAge($now = 'NOW') {
        return $this->diff($now)->format('%y');
    }
}
?>