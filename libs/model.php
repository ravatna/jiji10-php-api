<?php

abstract class AModel {
    abstract public function select();
    abstract public function insert();
    abstract public function update();
    abstract public function delete();
}

class ADataModel {
    /**
     * Identity of data
     * @param integer
     */
    public $id; 
    
    /**
     * Created at datetime
     */
    public $createdAt;

    /**
     * Updated at datetime
     */
    public $updatedAt;

    /**
     * Created by username
     */
    public $createdBy;

    /**
     * Updated by username
     */
    public $updatedBy;
}

class ColumnModel {
    private $id = 0;
    private $name = "";
    private $datatype = "varchar";
    private $size = 50;
} // .End class

class DbManagement extends AModel{
    public $wheres = array();
    public $columns = array();
    public $values = array();
    public $table_name = "";
    public $pk_id = "";
    protected $defaultColumns = array();

    public $Db;

    function __construct($h,$u,$p,$db){ 
        $this->Db = new MysqlClass($h,$u,$p,$db,3306,'/tmp/mysql.sock');
        
    }// .End 

    function getCreateCommand(){

        $this->columns = $this->defaultColumns;
    
    echo "<pre>";
    echo $this->createCMD();
    echo "<pre />";

}
 
    private function getCreateCMD(){
        
        if(count($this->columns) < 1){
            return "";
        }

        $sql = "create table {$this->table_name} (";
        foreach($this->columns as $k => $v){
            $sql .= "`{$v}` varchar(50) ,";
        } // .End foreach
        
        $sql = substr($sql,0,strlen($sql)-1);
        $sql .= ");";
        
        //echo $sql;
        
        return $sql;

    } // .End getSelectCMD

    private function getSelectCMD(){
        
        if(count($this->columns) < 1){
            return "";
        }

        $sql = "select ";
        foreach($this->columns as $k => $v){
            $sql .= "`{$v}`,";
        } // .End foreach
        
        $sql = substr($sql,0,strlen($sql)-1);
        
        $sql .= " from {$this->table_name} ";

        if(count($this->wheres) > 0) {
            $sql .= " where ";
            foreach($this->wheres as $k => $v){
                $sql .= "`{$k}` = '{$v}' and ";
            } 

            $sql = substr($sql,0,strlen($sql)-4);
        } // .End if
        
        //echo $sql;
        
        return $sql;

    } // .End getSelectCMD


    private function getUpdateCMD(){
        
        if(count($this->columns) < 1){
            return "pls, input columns";
        }

        if(count($this->values) < 1){
            return "pls, input values";
        }

        if(count($this->columns) != count($this->values)){
            return "num columns not equal num values";
        }

        $sql = "update {$this->table_name} set ";
        foreach($this->columns as $k => $v){
            $sql .= "{$v} = '{$this->values[$k]}',";
        } // .End foreach
        
        $sql = substr($sql,0,strlen($sql)-1);
        

        $sql .= " where ";
        foreach($this->wheres as $k => $v){
            $sql .= "{$k} = '{$v}' and ";
        } 

        $sql = substr($sql,0,strlen($sql)-4);
        return $sql;
    } // .End getUpdateCMD

    private function getInsertCMD(){
        
        $sql = "insert into {$this->table_name} ( ";
        foreach($this->columns as $k => $v){
            $sql .= "{$v},";
        }

        $sql = substr($sql,0,strlen($sql)-1);
        $sql .=") ";

        $sql .= " values ( ";
        foreach($this->values as $k => $v){
            $sql .= "'{$v}',";
        }
        $sql = substr($sql,0,strlen($sql) - 1);
        $sql .="); ";

        return $sql;

    } // .End getSelectCMD


    private function getDeleteCMD(){
        // delete from {table_name} where {pk_id}

        if(count($this->wheres) < 1){
            return "pls, input where key to delete.";
        }

        $sql = "delete ";
        $sql .= " from {$this->table_name} "; 
        $sql .= " where ";
        foreach($this->wheres as $k => $v){
            $sql .= "{$k} = '{$v}' and ";
        }

        $sql = substr($sql,0,strlen($sql)-4);
        
        return $sql;
    } // .End getDeleteCMD

    /**
     * Return result set
     */
    public function select(){
        $arr = array();
        $this->Db->connect();
        if($this->Db->isConnected()){
            //echo $this->getSelectCMD();
            $this->Db->query($this->getSelectCMD());    
            if(count($this->Db->getErr())>0) {
                print_r($this->Db->getErr());
            }
            $this->Db->close();
            return $this->Db->getResult();
        } // .End if

        return ;
    }// .End select()

    /**
     * Insert data
     */
    public function insert(){
        $arr = array();
        $this->Db->connect();
        if($this->Db->isConnected()){
            //echo $this->getInsertCMD();
            $this->Db->nonQuery($this->getInsertCMD());    
            $this->Db->close();
            return $this->Db->getEffected();
        } // .End if

        return ;
    } // .End insert()

    /**
     * Update data
     */
    public function update(){
        $arr = array();
        $this->Db->connect();
        if($this->Db->isConnected()){
            //echo $this->getUpdateCMD();
            $this->Db->nonQuery($this->getUpdateCMD());    
            $this->Db->close();
            return $this->Db->getEffected();
        } // .End if

        return ;
    } // .End update()

    /**
     * Delete data
     */
    public function delete(){
        $arr = array();
        $this->Db->connect();
        if($this->Db->isConnected()){
             
            $this->Db->nonQuery($this->getDeleteCMD());    
            $this->Db->close();
            return $this->Db->getEffected();
        } // .End if

        return ;
    } // .End delete()

    /**
     * Get Sql Command create table.
     */
    public function createCMD(){
        echo $this->getCreateCMD();
    } // .End delete()



    /**
     * Run command with raw command.
     */
    public function rawQuery($sql){

    } // .End rawQuery()


    function findAll($columns = array()){
        if(count($columns) > 0){
            $this->columns = $columns;
        }else{
            $this->columns = $this->defaultColumns;
        }
        
        $this->wheres = array();
        
        $result = $this->select();
        $my_data = array();
        while($a = $this->Db->fetchArray()){
            $aa = array();
            
            foreach($a as $k => $v){
                $aa[$k] = $v;
            }
            
            $my_data[] = $aa;
        } // .End while
        
        return $my_data;
    } // .End findAll

    function findById($id,$columns = array()){
        if(count($columns) > 0){
            $this->columns = $columns;
        }else{
            $this->columns = $this->defaultColumns;
        }
        
        $this->wheres = array("id"=> $id);
        $result = $this->select();
        $my_data = array();
        while($a = $this->Db->fetchArray()){
            $aa = array();    
            foreach($a as $k => $v){
                $aa[$k] = $v;
            }
            $my_data[] = $aa;
        } // .End while
        return $my_data;
    } // .End findById

    /**
     * Check duplication by column target
     */
    function isDuplicate($columns = array()){

        $this->columns = array();
        $this->wheres = array();

        foreach($columns as $k => $v) {
            $this->columns[] = $k;
            $this->wheres[$k] = $v;
        }
        
        $result = $this->select();
    
        $b = false;
        if($result->num_rows) {
            $b = true;
        }
        return $b;
    } // .End isDuplicate

    /**
     * Add new data
     */
    function add($columns = array(),$values = array()){
        if(count($columns) > 0){
            $this->columns = $columns;
            $this->values = $values;
        }
        $result = $this->insert();
        return $result;
    }

    function save($id,$columns = array(),$values = array()){
        if(count($columns) > 0){
            $this->columns = $columns;
            $this->values = $values;
            $this->wheres = array("{$this->pk_id}" => $id);
        }
  
        $result = $this->update();
        return $result;
    }


    function del($id){
        $this->wheres = array("{$this->pk_id}" => $id);
        $result = $this->delete();
        return $result->num_rows;
    }
}
 
