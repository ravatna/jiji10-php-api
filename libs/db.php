<?php
abstract class Database {
    protected $_uname;
    protected $_pword;
    protected $_serverIpName;
    protected $_db;

    function __construct($h,$u,$p,$db){
        $this->_uname = $u;
        $this->_pword = $p;
        $this->_serverIpName = $h;
        $this->_db = $db;
    }

    /**
     * Do open connection with mysql database server
     */
    abstract function connect();

    /**
     * Return result record sets
     */
    abstract function query($sql);
    
    /**
     * Return affected numrow
     */
    abstract function nonQuery($sql);
    
} // .End class

/**
 * Class to manage database mysql server
 */
class MysqlClass extends Database{

    private $_connect;
    private $_result;
    private $_err = array();
    private $num_rows = 0;
    private $_sql_log = "sql";
    private $_sql_error_log = "sql_error";

    /**
     * num of record to effect data
     */
    private $_effected = -1;

    function __construct($h,$u,$p,$db){
        parent::__construct($h,$u,$p,$db);   
    }

    public function getErr(){
        return $this->_err;
    }

    /**
     * Open connection to database mysql server
     */
    function connect(){
         $this->_connect = mysqli_connect($this->_serverIpName,$this->_uname,$this->_pword,$this->_db,3306,'/tmp/mysql.sock');
         //mysqli_set_charset($this->_connect, "utf8");
         
    } // .End connect()

    /**
     *  
     */
    function isConnected(){
        if($this->_connect){
            return true;
        }else{
            return false;
        }
    } // .End isConnected()
    
    /**
     * query to mysql server
     */
    function query($sql){
        $this->_result = mysqli_query($this->_connect,$sql,MYSQLI_STORE_RESULT);
        LogFile::log($this->_sql_log, $sql);
        if($this->_result){
            $this->_num_rows = mysqli_num_rows($this->_result);
        }else{
            $this->_err = mysqli_error_list($this->_connect);
        }
        
    }// .End query()

    function result_free(){
        $this->_result = null;
    }

    function getNumRows(){
        return $this->_num_rows;
    }

    /**
     * Return query result  
     */
    function getResult(){
        return $this->_result;
    } // .End getResult()

    /**
     * Return top one array
     */
    function fetchArray(){
        return mysqli_fetch_array($this->_result, MYSQLI_ASSOC);
    } // .End fetchArray()

    /**
     * Return numrow  for manipulate sql command
     */
    function getEffected(){
        return $this->_effected;
    } // .End getEffected()

    /**
     * Query in case manipulate sql command crate update delete
     */
    function nonQuery($sql){
        $this->_result = mysqli_query($this->_connect, $sql,MYSQLI_STORE_RESULT);
        LogFile::log($this->_sql_log, $sql);
        if($this->_result){
            $this->_effected = mysqli_affected_rows($this->_connect);
            
        }else{
            $this->_err = mysqli_error_list($this->_connect);
            LogFile::log($this->_sql_error_log, $sql);
        }

    } // .End nonQuery()

    /**
     * Return boolean
     */
    function close(){
        return mysqli_close($this->_connect);
    }

} // .End class