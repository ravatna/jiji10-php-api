<?php
/**
 * Class to prepare array to JSON format
 */
class EncJSON {
    private $arr;

    function __construct(){

    } // .End __construct()

    function getArray(){
        return $this->arr;
    }

    /**
     * Input array without manage position
     */
    function add($v){
        if (is_a($v, "EncJSON")){
            $this->arr[] = $v->getArray();
        }else{
            $this->arr[] = $v;
        }
    } // .End add()

    /**
     * Input array with key position
     * @param $k string
     * @param $v mix
     */
    function addWithKey($k,$v){

        if (is_a($v, "EncJSON")){
            $this->arr[$k] = $v->getArray();
        }else{
            $this->arr[$k] = $v;
        }
    } // .End addWithKey()

    

    /**
     * Magic function to encode array to JSON String
     */
    public function __toString(){
        return json_encode($this->arr);
    } // .End __string()
    
    /**
     * Display data to screen
     */
    public function echo(){
        echo $this->__toString();
    }

} // .End class