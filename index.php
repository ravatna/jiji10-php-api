<?php
define("JIJI_CODE_VERSION","0.1-dev");
define("JIJI_DEBUG_MODE",-1);
define("JIJI_PRODUCT_MODE",0);

// require librarys 
require_once("libs/jijidatetime.php");
require_once("libs/model.php");
require_once("libs/encjson.php");
require_once("libs/db.php");
require_once("libs/thsms.php");

// Data model
require_once("models/card.php");
require_once("models/member.php");
require_once("models/cardmember.php");
require_once("models/card.php");
require_once("models/redeemitem.php");
require_once("models/banner.php");
require_once("models/otp.php");
require_once("models/card_of_member.php");


class LogFile{

    /**
     * Write log content to log file.
     */
    public static function log($file_name, $s){
        $log_name = "{$file_name}_".date("Ymd_H").".log";
        $log_time = "[" . date("Y-m-d H:i:s") . "]" . $s."\n";
        $fp = fopen("log/{$log_name}","a+");
        fwrite($fp,$log_time,strlen($log_time));
        fclose($fp);
    }// .End log()
} // .End LogFile class

/**
 * return display data to developer and start modify programs
 */
class Debug {
    public static function print($s){
        echo $s ."<br/>";
    } // .End print()
} // .End Debug

/**
 * Manage error to easy to understand
 */
class ErrorClass {
    private $_errMsg = "";
    private $_errNo = -1;

    public function __toString(){ 
        if ($this->_err != -1){
            echo "Error Code : {$this->_errNo}, Error Message : {$this->errMsg}";
        } // .End if
    }
} // .End class

/**
 * Class to access php parameter in php environment
 */
class HttpParams {
    /**
     * Return data from $_POST parameter
     * if not exist param return empty
     */
    public static function post($k){
        if(array_key_exists($k,$_POST)){
            return $_POST[$k];
        }else{
            return "";
        }
        
    } // .End post()

    public static function getAll(){
        return $_GET;
    } // .End static getAll()

    public static function postAll(){
        return $_POST;
    } // .End static postAll()

    public static function fileAll(){
        return $_FILES;
        
    } // .End static fileAll()

    public static function requestAll(){
        return $_REQUEST;
    } // .End static requestAll()

    /**
     * Return data from $_GET parameter
     * if not exist param return empty
     */
    public static  function get($k){
        if(array_key_exists($k,$_GET)){
            return $_GET[$k];
        }else{
            return "";
        }
    } // .End get()

    /**
     * Return data from $_FILE parameter
     * if not exist param return empty
     */
    public static function file($k){
        if(array_key_exists($k,$_FILES)){
            return $_FILES[$k];
        }else{
            return "";
        }
    } // .End file()

    /**
     * Return data from $_REQUEST parameter
     * if not exist param return empty
     */
    public static  function request($k){
        if(array_key_exists($k,$_REQUEST)){
            return $_REQUEST[$k];
        }else{
            return "";
        }
    } // .End file()
} // .End class 


/**
 * Class to setup basig return json output for JIJI10 Class
 */
class ResJSON {
    private $arr;
    private $jsonObj;
    private $status = "";
    private $msg = "";
    
    function __construct ($env=JIJI_DEBUG_MODE){
        $this->arr['msg'] = $this->msg;
        $this->arr['status'] = $this->status;
        
        if($env==JIJI_DEBUG_MODE){
            $this->arr['get'] = HttpParams::getAll();
            $this->arr['post'] = HttpParams::postAll();
            $this->arr['file'] = HttpParams::fileAll();
        } // .End if
        
    } // .End __construct

    public function statusOK(){
        $this->arr['status'] = "OK";
    }

    public function statusFail(){
        $this->arr['status'] = "FAIL";
    }

    public function msg($s){
        $this->arr['msg'] = $s;
    }

    /**
     * set data to base key name is default
     * @param array|string|int $a
     */
    public function data($a) {
        $this->arr['data'] = $a;
    }

    public function echo(){
        $this->jsonObj = new EncJSON();
        $this->jsonObj->add($this->arr);
        $this->jsonObj->echo();
    }

    public function __toString(){
        $this->jsonObj = new EncJSON();
        $this->jsonObj->add($this->arr);
        return $this->jsonObj->__toString();
    }

   
} // .End class

/**
 * Class for manage royaty program 
 * Engine to control system 
 */
class Jiji10 {
    private $_VESION_ = JIJI_CODE_VERSION;
    private $_event_log = "event";
    private $_error_log = "app_err";

    
    /**
     * @member EncJSON
     */
    private $resObj; 

    /**
     * default method to run web service
     */
    private $method = "index";

    /**
     * Integer
     */
    public  $IS_ENV = JIJI_DEBUG_MODE;
    

    /**
     * Constructor
     */
    function __construct($env = JIJI_DEBUG_MODE){
        
        $this->setENV($env);
        $this->resObj = new ResJSON($env);
        $this->method = $_GET['a'];
        $this->resultJson =  new EncJSON();

    } // End Jiji10()

    function version(){
        return $this->_VERSION_;
    }

    function eventLog($msg,$method = ""){
        LogFile::log($this->_event_log, $method . ":" . $msg);
    }

    function errorLog($msg,$method = ""){
        LogFile::log($this->_error_log, $method . ":" . $msg);
    }

    /**
     * 
     */
    function setENV($mode){
        $this->IS_ENV = $mode;

    } // .End setENV()


    /**
     * Register new person to system.
     */
    private function doRegisterMember() {


        /**
         * New user
         * view get phone number
         * 1. input mobile phone 
         * 
         * view input user info
         * 2. Get otp and input otp
         * 3. Input username password
         * 4. Name & Surname
         */

        $mobile = HttpParams::post("mobile");
        $email = HttpParams::post("email");
        $uname = HttpParams::post("uname");
        $pword = HttpParams::post("pword");

        // if require data is not empty then 
        if($mobile !="" && $email !="" && $uname !="" && $pword!=""){

            // $name = HttpParams::get("name");
            // $surname = HttpParams::get("surname");
            // $idcard = HttpParams::get("idcard");
            // $bday = HttpParams::get("bday");
            // $addr = HttpParams::get("addr");
            // $city = HttpParams::get("city");
            // $district = HttpParams::get("district");
            // $province = HttpParams::get("province");
            // $zipcode = HttpParams::get("zipcode");

            $name = "";
            $surname = "";
            $idcard = "";
            $bday = "";
            $addr = "";
            $city = "";
            $district = "";
            $province = "";
            $zipcode = "";
            
            // create MemberManage object to manage
            $m = new MemberManage();
            $cols = array("phone" => $mobile);
            $cols2 = array("email" => $email);
            
            if($m->isDuplicate($cols) == false) {
                if($m->isDuplicate($cols2) == false){
                    $now = new DateTime();
                    
                    $age = 0; 
                    $is_lock = 0;
                    $created_at = $now->format("Y-m-d H:i:s"); 
                    $created_by = "system"; 
                    $updated_at = $now->format("Y-m-d H:i:s");
                    $updated_by = "system"; 
                    $cols = array( "fname", "lname", "email", "phone", "uname", "pword", "bday", "age", "addr", "city", "district", "province", "zipcode", "is_lock", "created_at", "created_by", "updated_at", "updated_by");
                    $vals = array( $name, $surname, $email, $mobile, $uname, $pword, $bday, $age, $addr, $city, $district, $province, $zipcode, $is_lock, $created_at, $created_by, $updated_at, $updated_by);
                    $m->add($cols,$vals);
                    
                    $this->resObj->statusOK();    
                    $this->resObj->msg("data register success.");
                    $this->resObj->data($data);
                    $this->eventLog("register new member to system. ",__METHOD__);

                }else{

                    $this->resObj->statusFail();    
                    $this->resObj->msg("data email duplicate.");
                    $this->errorLog("data email duplicate.",__METHOD__);
                }
                    
            }else{
                $this->resObj->statusFail();    
                $this->resObj->msg("data phone number duplicate.");
                $this->errorLog("data phone number duplicate.",__METHOD__);
            }
        
        }else{
            $this->eventLog("require field is empty mobile:{$mobile}, email: {$email}, uname: {$uname}, pword: {$pword}. ",__METHOD__);
        } // .End if require field not empty

        return $this->resObj->__toString();
    

    } // .End doRegisterMember()



    /**
     * 
     * @param $cardNo
     * @param $mobile
     */
    private function doRegisterMemberByCard($cardNo,$mobile) {


        /**
         * New user
         * view get phone number
         * 1. input mobile phone 
         * 
         * view input user info
         * 2. Get otp and input otp
         * 3. Input username password
         * 4. Name & Surname
         */

        $uname = $cardNo;
        $pword = $mobile;
        $name = "";
        $surname = "";
        $idcard = "";
        $bday = "";
        $addr = "";
        $city = "";
        $district = "";
        $province = "";
        $zipcode = "";
        $email = "";
        // if require data is not empty then 
        if($uname !="" && $pword!=""){

            // create MemberManage object to manage
            $m = new MemberManage();
            

            // $cols = array("phone" => $mobile, "card_no" => $card_no);
            
            
            // if($m->isDuplicate($cols) == false) {
                $now = new DateTime();
                
                $age = 0; 
                $is_lock = 0;

                $created_at = $now->format("Y-m-d H:i:s"); 
                $created_by = "system"; 
                $updated_at = $now->format("Y-m-d H:i:s");
                $updated_by = "system"; 
                
                $cols = array( "fname", "lname", "email", "phone", "uname", "pword", "bday", "age", "addr", "city", "district", "province", "zipcode", "is_lock", "created_at", "created_by", "updated_at", "updated_by");
                $vals = array( $name, $surname, $email, $mobile, $uname, $pword, $bday, $age, $addr, $city, $district, $province, $zipcode, $is_lock, $created_at, $created_by, $updated_at, $updated_by);
                
                $m->add($cols,$vals);
                
                $this->eventLog("register new member to system. ",__METHOD__);


                return true;
            // }else{              
            //     $this->errorLog("data phone number duplicate , card no duplicate.",__METHOD__);
            //     return false;
            // }
        
        }else{
            $this->eventLog("require field is empty mobile:{$mobile}, email: {$email}, uname: {$uname}, pword: {$pword}. ",__METHOD__);
            return false;
        } // .End if require field not empty

      

    } // .End doRegisterMemberByCard()

    /**
     * Do register card 2 app
     */
    private function doRegisterCard2App() {

        /**
         * New user
         * view get phone number
         * 1. input card no phone 
         
         * 
         * view input user info
         * 2. Get otp and input otp
         * 3. Input username password
         * 4. Name & Surname
         */

        $card_no = HttpParams::get("card_no");
        $mobile = HttpParams::get("mobile"); 
        
        $data = $this->GetCardInfo($card_no);
        
        $cm = new CardMemberManage();
        $cols = array("CardNumber" => $card_no);
        if($cm->isDuplicate($cols) == false ){
            $this->eventLog("found duplicate Card Number {$card_no}",__METHOD__);

            $CardNumber = $data->CardNumber;
            $IdentifierNumber = $data->IdentifierNumber;
            $TelephoneNumber = $data->TelephoneNumber;
            $CardType = $data->CardType;
            $StartDate = explode(" ",$data->StartDate)[0];
            $ExpireDate = explode(" ",$data->ExpireDate)[0];
            $PercentDiscount = $data->PercentDiscount;
            $CardValue = $data->CardValue;
            
            $Point = $data->Point; // use with neo suki
            $CashCoupon = $data->CashCoupon; // use with neo suki

            $TitleName = $data->TitleName;
            $GivenName = $data->GivenName;
            $FamilyName = $data->FamilyName;
            $CustomerID = $data->CustomerID;
            $Birthdate = $data->Birthdate;
            $Active = $data->Active;
            $Result = $data->Result;
            $Message = $data->Message;
            $MarryDate = explode(" ",$data->MarryDate)[0];
            $ApplyMarryDateGift = $data->ApplyMarryDateGift;
            $ApplyBirthdateGift = $data->ApplyBirthdateGift;

            // result true value
            if($Result == 0){

                // do register data to mobile crm
                if($this->doRegistermemberByCard($card_no,$mobile)){

                    $m = new MemberManage();
                    $res = $m->findByMobile($mobile);
                    $member_id = $res[0]['id'];

                    $cols = array( 
                        "CardNumber", "IdentifierNumber", "TelephoneNumber", "CardType", "StartDate", 
                        "ExpireDate", "PercentDiscount", "CardValue", "Point", "CashCoupon", "TitleName", 
                        "GivenName", "FamilyName", "CustomerID", "Birthdate", "Active", "Result", 
                        "Message","MarryDate","ApplyMarryDateGift","ApplyBirthdateGift","member_id");

                        $vals = array( 
                            $CardNumber, $IdentifierNumber, $TelephoneNumber, $CardType, $StartDate, 
                            $ExpireDate, $PercentDiscount, $CardValue, $Point, $CashCoupon, $TitleName, 
                            $GivenName, $FamilyName, $CustomerID, $Birthdate, $Active, $Result, 
                            $Message,$MarryDate,$ApplyMarryDateGift,$ApplyBirthdateGift,$member_id);

                    $vv = $cm->add($cols,$vals);
 
                    if($vv > 0){
                        $com = new CardOfMemberManage();
                        // add transction item
                        $now = new DateTime();
                        $created_at = $now->format("Y-m-d H:i:s"); 
                        $created_by = "system"; 
                        $updated_at = $now->format("Y-m-d H:i:s");
                        $updated_by = "system"; 

                        $cols = array("member_id", "card_no", "created_at", "created_by", "updated_at", "updated_by");
                        $vals = array($member_id, $card_no, $created_at, $created_by, $updated_at, $updated_by);
                        $com->add($cols,$vals);
                        $this->eventLog("register new member to card_of_member. ",__METHOD__);

                        // send otp
                        $arr = $this->doSendOTP($mobile);
                        $data->ref_no = @$arr['ref_no'];
                        
                        $this->resObj->statusOK();
                        $this->resObj->msg("add data from crm success.");
                        $this->eventLog("add data success");

                    }else{
                        $this->resObj->statusFail();    
                        $this->resObj->msg("error can not register CardNumber {$card_no}");
                        $this->eventLog("error can not register CardNumber {$card_no}");
                    }
                }else{
                    $this->resObj->statusFail();    
                    $this->resObj->msg("can not register member");
                    $this->eventLog("can not register member");
                }

            }else{
                $this->resObj->statusFail();    
                $this->resObj->msg("invalid card no");
                $this->eventLog("invalud card no");
            }

            $data->member_id = @$member_id;
            $this->resObj->data($data);

        } else  {
            //echo "found card no";
            $this->resObj->statusFail();    
                $this->resObj->msg("error found duplicate CardNumber {$card_no}");
                $this->eventLog("error found duplicate CardNumber {$card_no}");
        }

        return $this->resObj->__toString();

    } // .End doRegisterCard2App()


    private function doRegisterCard2App__backup() {

        /**
         * New user
         * view get phone number
         * 1. input card no phone 
         
         * 
         * view input user info
         * 2. Get otp and input otp
         * 3. Input username password
         * 4. Name & Surname
         */

        $card_no = HttpParams::get("card_no"); 
        $data = $this->GetCardInfo($card_no);
        
        $cm = new CardMemberManage();
        $cols = array("CardNumber" => $card_no);
        if($cm->isDuplicate($cols) == false ){
            $this->eventLog("found duplicate CardNumber {$card_no}",__METHOD__);

            $CardNumber = $data->CardNumber;
            $IdentifierNumber = $data->IdentifierNumber;
            $TelephoneNumber = $data->TelephoneNumber;
            $CardType = $data->CardType;
            $StartDate = explode(" ",$data->StartDate)[0];
            $ExpireDate = explode(" ",$data->ExpireDate)[0];
            $PercentDiscount = $data->PercentDiscount;
            $CardValue = $data->CardValue;
            
            $Point = $data->Point; // use with neo suki
            $CashCoupon = $data->CashCoupon; // use with neo suki

            $TitleName = $data->TitleName;
            $GivenName = $data->GivenName;
            $FamilyName = $data->FamilyName;
            $CustomerID = $data->CustomerID;
            $Birthdate = $data->Birthdate;
            $Active = $data->Active;
            $Result = $data->Result;
            $Message = $data->Message;
            $MarryDate = explode(" ",$data->MarryDate)[0];
            $ApplyMarryDateGift = $data->ApplyMarryDateGift;
            $ApplyBirthdateGift = $data->ApplyBirthdateGift;

            $vals = array( 
                $CardNumber, $IdentifierNumber, $TelephoneNumber, $CardType, $StartDate, 
                $ExpireDate, $PercentDiscount, $CardValue, $Point, $CashCoupon, $TitleName, 
                $GivenName, $FamilyName, $CustomerID, $Birthdate, $Active, $Result, 
                $Message,$MarryDate,$ApplyMarryDateGift,$ApplyBirthdateGift);

            $cols = array( 
                "CardNumber", "IdentifierNumber", "TelephoneNumber", "CardType", "StartDate", 
                "ExpireDate", "PercentDiscount", "CardValue", "Point", "CashCoupon", "TitleName", 
                "GivenName", "FamilyName", "CustomerID", "Birthdate", "Active", "Result", 
                "Message","MarryDate","ApplyMarryDateGift","ApplyBirthdateGift");

            if($cm->add($cols,$vals) > 0){
                $this->resObj->statusOK();
                $this->resObj->msg("add data from crm success.");

                $this->eventLog("add data success");
            }else{
                $this->eventLog("found duplicate CardNumber {$card_no}");
            }
            
            //$this->resObj->data($data);

        } else  {
            echo "found card no";
        }

        return $this->resObj->__toString();

    } // .End doRegisterCard2App()


    /**
     * Update member
     */
    private function doMemberUpdate() {
        // require id value
        // not update username, password, email, birthday, mobile

        $id = HttpParams::get("member_id"); // pk

        // create MemberManage object to manage
        $m = new MemberManage();

        // find data by id (pk)
        $test = $m->findById($id);
        
        if( count($test) > 0){
            $card_no = HttpParams::get("card_no");
            $mobile = HttpParams::get("mobile");
            
            $name = HttpParams::get("name");
            $surname = HttpParams::get("surname");
            
            $bday = "";
            $email = HttpParams::get("email");

            $addr = "";
            $city = "";
            $district = "";
            $province = "";
            $zipcode = "";

            $now = new DateTime();
            $age = 0;
            $is_lock = 0;

            $updated_at = $now->format("Y-m-d H:i:s");
            $updated_by = "system";

            $cols = array( "fname", "lname", "email",   "updated_at", "updated_by");
            $vals = array( $name, $surname, $email,  $updated_at, $updated_by);
            
            //$m->save($id,$cols,$vals);

            if($m->save($id,$cols,$vals)){
                // $this->updateCRMClexpert_Member();

                $this->resObj->statusOK();
                $this->resObj->msg("data update success.");
                //$this->resObj->data($data);
            }else{
                $this->resObj->statusFail();    
                $this->resObj->msg("data not update member.");
            }
            
        }else{
            $this->resObj->statusFail();    
            $this->resObj->msg("data not found member.");
        }
        
        return $this->resObj->__toString();

    } // .End doMemberUpdate()



    /**
     * Update member
     */
    private function doMemberUpdate_backup() {
        // require id value
        // not update username, password, email, birthday, mobile

        $id = HttpParams::get("id"); // pk

        // create MemberManage object to manage
        $m = new MemberManage();

        // find data by id (pk)
        $test = $m->findById($id);
        
        if( count($test) > 0){

            $mobile = HttpParams::get("mobile");
            $email = HttpParams::get("email");
            
            $name = HttpParams::get("name");
            $surname = HttpParams::get("surname");
            
            $bday = HttpParams::get("bday");

            $addr = HttpParams::get("addr");
            $city = HttpParams::get("city");
            $district = HttpParams::get("district");
            $province = HttpParams::get("province");
            $zipcode = HttpParams::get("zipcode");

            $now = new DateTime();
            $age = 0;
            $is_lock = 0;

            $updated_at = $now->format("Y-m-d H:i:s");
            $updated_by = "system";

            $cols = array( "fname", "lname", "email", "phone",  "bday", "age", "is_lock", "updated_at", "updated_by");
            $vals = array( $name, $surname, $email, $mobile, $bday, $age , $is_lock,  $updated_at, $updated_by);
            
            if($m->save($id,$cols,$vals)){
                $this->updateCRMClexpert_Member();

                $this->resObj->statusOK();
                $this->resObj->msg("data update success.");
                $this->resObj->data($data);
            }else{
                $this->resObj->statusFail();    
                $this->resObj->msg("data not update member.");
            }

        }else{
            $this->resObj->statusFail();    
            $this->resObj->msg("data not found member.");
        }
        
        return $this->resObj->__toString();

    } // .End doMemberUpdate_backup()

    

    /**
     * Update
     */
    private function updateCRMClexpert_Cards(){
        $server = "192.168.0.34";
        $u = "sa";
        $p = "qaz1wsx2";

        $serverName = "192.168.0.34";
        $connectionInfo = array( "Database"=>"CLCRM2", "UID"=>"sa", "PWD"=>"qaz1wsx2" );
        $conn = sqlsrv_connect( $serverName, $connectionInfo);
        if( $conn === false ) {
            die( print_r( sqlsrv_errors(), true));
        }

        $ID=HttpParams::get('ID');
        $CardNumber=HttpParams::get('CardNumber');
        $CardTypeCode=HttpParams::get('CardTypeCode');
        $CardTypeName=HttpParams::get('CardTypeName');
        $CardPercentDiscount=HttpParams::get('CardPercentDiscount');
        $Term=HttpParams::get('Term');
        $StartDate=HttpParams::get('StartDate');

        $sql = "insert into Customers (Code,FirstName,LastName,LocalFullName,SexCode,IdentifierNumber,MobileNumber,UseCardPromotion,CompletedInformation,MobileUser) values({$Code}','{$FirstName}','{$LastName}','{$LocalFullName}','{$SexCode}','{$IdentifierNumber}','{$MobileNumber}','{$UseCardPromotion}','{$CompletedInformation}','{$MobileUser}');";
         
        $stmt = sqlsrv_query( $conn, $sql, $params);
        sqlsrv_close($conn);

        if( $stmt !== false ) {
            die( print_r( sqlsrv_errors(), true));
            $this->resObj->statusOK();
            $this->resObj->msg("data update success.");
            $this->resObj->data($data);
        }else{
            $this->resObj->statusFail();    
            $this->resObj->msg("data not update member.");
        }        
    } // .End updateCRMClexpert_Cards()


    private function updateCRMClexpert_Customers(){
        $server = "192.168.0.34";
        $u = "sa";
        $p = "qaz1wsx2";


        $serverName = "192.168.0.34";
        $connectionInfo = array( "Database"=>"CLCRM2", "UID"=>"sa", "PWD"=>"qaz1wsx2" );
        $conn = sqlsrv_connect( $serverName, $connectionInfo);
        if( $conn === false ) {
            die( print_r( sqlsrv_errors(), true));
        }

        $ID=HttpParams::get('ID');
        $CardNumber=HttpParams::get('CardNumber');
        $CardTypeCode=HttpParams::get('CardTypeCode');
        $CardTypeName=HttpParams::get('CardTypeName');
        $CardPercentDiscount=HttpParams::get('CardPercentDiscount');
        $Term=HttpParams::get('Term');
        $StartDate=HttpParams::get('StartDate');
        $ExpireDate=HttpParams::get('ExpireDate');
        $ValueBalance=HttpParams::get('ValueBalance');
        $FreeCash=HttpParams::get('FreeCash');
        $Point=HttpParams::get('Point');
        $CashCoupon=HttpParams::get('CashCoupon');
        $LotNumber=HttpParams::get('LotNumber');
        $LotDate=HttpParams::get('LotDate');
        $FromFactory=HttpParams::get('FromFactory');
        $IssueOutlet=HttpParams::get('IssueOutlet');
        $Active=HttpParams::get('Active');
        $ReUse=HttpParams::get('ReUse');
        $ReIssueApproved=HttpParams::get('ReIssueApproved');
        $CPlayCardNumber=HttpParams::get('CPlayCardNumber');


        $sql = "UPDATE  Customers SET  
        


        WHERE [CustomerID] = '{$CustomerID}'
        ";
        
        $stmt = sqlsrv_query( $conn, $sql, $params);
        sqlsrv_close($conn);

        if( $stmt !== false ) {
            die( print_r( sqlsrv_errors(), true));
            $this->resObj->statusOK();
            $this->resObj->msg("data update success.");
            $this->resObj->data($data);
        }else{
            $this->resObj->statusFail();    
            $this->resObj->msg("data not update member.");
        }  
    } // .End updateCRMClexpert_Customers()

    private function queryMSSQL(){
        $server = "192.168.0.34";
        $u = "sa";
        $p = "qaz1wsx2";

        $serverName = "192.168.0.34\SQLEXPRESS";
        $connectionInfo = array( "Database"=>"CLCRM2", "UID"=>"sa", "PWD"=>"qaz1wsx2" );
        $conn = sqlsrv_connect( $serverName, $connectionInfo);
        if( $conn === false ) {
            die( print_r( sqlsrv_errors(), true));
        }


       $mobile = HttpParams::get("mobile");
    //    $card_no = HttpParams::get("card_no");


        $sql = "select * from Customers where Phone='{$mobile}'";
        // $sql = "select * from Cards where CardNumber='{$card_no}'";
        // echo $sql;
        $stmt = sqlsrv_query( $conn, $sql);

        //var_dump($stmt);

        while($result = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
        {
            foreach($result as $k => $v){
                echo $k ."='{\$".$k."}',<br/>";
            }

            foreach($result as $k => $v){
                echo "\$".$k ."=HttpParams::get('".$k."');<br/>";
            }
            
        }

        sqlsrv_close($conn);

        if( $stmt !== false ) {
            die( print_r( sqlsrv_errors(), true));
            $this->resObj->statusOK();
            $this->resObj->msg("data update success.");
            $this->resObj->data($data);
        }else{
            $this->resObj->statusFail();    
            $this->resObj->msg("data not update member.");
        }

        
    } // .End query MSSQL


    /**
     * Delete data member
     */
    private function doMemberDelete() {
        // require id value
        // not update username, password, email, birthday, mobile

        $id = HttpParams::get("id"); // pk

        // create MemberManage object to manage
        $m = new MemberManage();

        // find data by id (pk)
        $test = $m->findById($id);

        if( count($test) > 0){ 
            $m->del($id);

            $this->resObj->statusOK();
            $this->resObj->msg("data delete success.");
            $this->resObj->data($data);
        }else{
            $this->resObj->statusFail();    
            $this->resObj->msg("data not found member.");
        }
        
        return $this->resObj->__toString();

    } // .End doMemberDelete()



    /**
     * Authen user and return json object in member
     */
    private function doLoginAuthen() {
        // get data from url query string
        $uname = HttpParams::get("uname");
        $pword = HttpParams::get("pword");

        // create MemberManage object to manage
        $m = new MemberManage();
        $my_data = $m->findByUnameAndPword($uname,$pword);
        
        // assign output to json format
        $this->resObj->statusOK();    
        $this->resObj->msg("data login success.");
        $this->resObj->data($my_data);

        return $this->resObj->__toString();
 
    } // .End doLoginAuthen

    /**
     * Return json object in format member card lists
     */
    private function doGetCardList() {
        $cm = new CardMemberManage();
        $m = new MemberManage();


        $res_m = $m->findByMobile(HttpParams::get("mobile"));  
        //print_r($res_m);
        $res = $cm->findCardByMemberId($res_m[0]['id']);

        $my_data = array();

        // loop from card list
        for($i =0; $i < count($res); $i++){
            $data = $this->GetCardInfo(trim($res[$i]['CardNumber']));
            
            $my_data[$i]['CardNumber'] = $data->CardNumber;
            $my_data[$i]['IdentifierNumber'] = $data->IdentifierNumber;
            $my_data[$i]['TelephoneNumber'] = $data->TelephoneNumber;
            $my_data[$i]['CardType'] = $data->CardType;
            $my_data[$i]['StartDate'] = explode(" ",$data->StartDate)[0];
            $my_data[$i]['ExpireDate'] = explode(" ",$data->ExpireDate)[0];
            $my_data[$i]['PercentDiscount'] = $data->PercentDiscount;
            $my_data[$i]['CardValue'] = $data->CardValue;
            $my_data[$i]['Point'] = $data->Point;
            $my_data[$i]['CashCoupon'] = $data->CashCoupon;
            // $my_data[$i]['TitleName'] = $data->TitleName;
            // $my_data[$i]['GivenName'] = $data->GivenName;
            // $my_data[$i]['FamilyName'] = $data->FamilyName;
            $my_data[$i]['CustomerID'] = $data->CustomerID;
            $my_data[$i]['Birthdate'] = $data->Birthdate;
            $my_data[$i]['Active'] = $data->Active;
            $my_data[$i]['Result'] = $data->Result;
            $my_data[$i]['Message'] = $data->Message;
            $my_data[$i]['MarryDate'] = explode(" ",$data->MarryDate)[0];
            
            $my_data[$i]['TitleName'] = "";
            $my_data[$i]['GivenName'] = $res_m[0]['fname'];
            $my_data[$i]['FamilyName'] = $res_m[0]['lname'];

        } // .End for
        
        $this->resObj->statusOK();    
        $this->resObj->msg("data card list success.");
        $this->resObj->data($my_data);

        return $this->resObj->__toString();
        
    } // .End doGetCardList


    private function doGetCardList_2() {
        $cm = new CardMemberManage();
        $m = new MemberManage();
        

        $res_m = $m->findByMobileAndCardNo(HttpParams::get("mobile"), HttpParams::get("card_no"));  
        
        $res = $cm->findCardByMemberId($res_m[0]['id']);

        $my_data = array();

        // loop from card list
        for($i =0; $i < count($res); $i++){
            $data = $this->GetCardInfo(trim($res[$i]['CardNumber']));
            
            $my_data[$i]['CardNumber'] = $data->CardNumber;
            $my_data[$i]['IdentifierNumber'] = $data->IdentifierNumber;
            $my_data[$i]['TelephoneNumber'] = $data->TelephoneNumber;
            $my_data[$i]['CardType'] = $data->CardType;
            $my_data[$i]['StartDate'] = explode(" ",$data->StartDate)[0];
            $my_data[$i]['ExpireDate'] = explode(" ",$data->ExpireDate)[0];
            $my_data[$i]['PercentDiscount'] = $data->PercentDiscount;
            $my_data[$i]['CardValue'] = $data->CardValue;
            $my_data[$i]['Point'] = $data->Point;
            $my_data[$i]['CashCoupon'] = $data->CashCoupon;
            // $my_data[$i]['TitleName'] = $data->TitleName;
            // $my_data[$i]['GivenName'] = $data->GivenName;
            // $my_data[$i]['FamilyName'] = $data->FamilyName;
            $my_data[$i]['CustomerID'] = $data->CustomerID;
            $my_data[$i]['Birthdate'] = $data->Birthdate;
            $my_data[$i]['Active'] = $data->Active;
            $my_data[$i]['Result'] = $data->Result;
            $my_data[$i]['Message'] = $data->Message;
            $my_data[$i]['MarryDate'] = explode(" ",$data->MarryDate)[0];
            
            $my_data[$i]['TitleName'] = "";
            $my_data[$i]['GivenName'] = $res_m[0]['fname'];
            $my_data[$i]['FamilyName'] = $res_m[0]['lname'];

        } // .End for
        
        $this->resObj->statusOK();    
        $this->resObj->msg("data card list success.");
        $this->resObj->data($my_data);

        return $this->resObj->__toString();
        
    } // .End doGetCardList

    /**
     * Delete data banner
     */
    private function doCardDelete() {
        // require id value
        // not update username, password, email, birthday, mobile

        $id = HttpParams::get("id"); // pk

        // create CardManage object to manage
        $m = new CardManage();

        // find data by id (pk)
        $test = $m->findById($id);

        if( count($test) > 0){ 
            $m->del($id);

            $this->resObj->statusOK();
            $this->resObj->msg("data delete success.");
            $this->resObj->data($data);
        }else{
            $this->resObj->statusFail();    
            $this->resObj->msg("data not found.");
        }
        
        return $this->resObj->__toString();

    } // .End doCardDelete()


    /**
     * Return json object in format member card info
     */
    private function doGetCardInfo() {
        $m = new CardMemberManage();

        $card_no = HttpParams::get("card_no");
        
        $this->GetCardInfo($card_no);
        
        // $test = $m->findById($card_no);

        // if( count($test) > 0){ 
        //     $this->resObj->statusOK();
        //     $this->resObj->msg("data get success.");
        //     $this->resObj->data($data);
        // }else{
        //     $this->resObj->statusFail();    
        //     $this->resObj->msg("data not found.");
        // }

        return $this->resObj->__toString();
        
    } // .End doGetCardInfo

    /**
     * Return json object in format Redeem item
     */
    private function doGetRedeemItems() {
        
        // create MemberManage object to manage
        $r = new RedeemItemManage();
        $my_data = $r->findAll();

        $this->resObj->statusOK();
        $this->resObj->msg("data redeem items success.");
        $this->resObj->data($my_data);

        return $this->resObj->__toString();
        
    } // .End doGetRedeemItems

    /**
     * Delete data redeem item
     */
    private function doRedeemItemDelete() {
        // require id value
        $id = HttpParams::get("id"); // pk

        // create RedeemItemManage object to manage
        $m = new RedeemItemManage();

        // find data by id (pk)
        $test = $m->findById($id);

        if( count($test) > 0){ 
            $m->del($id);

            $this->resObj->statusOK();
            $this->resObj->msg("data delete success.");
            $this->resObj->data($data);
        }else{
            $this->resObj->statusFail();    
            $this->resObj->msg("data not found.");
        }
        
        return $this->resObj->__toString();

    } // .End doBannerDelete()

    private function doGetBanners() {
        
        // create MemberManage object to manage
        $b = new BannerManage();
        $my_data = $b->findAll();

        $this->resObj->statusOK();    
        $this->resObj->msg("data banners success.");
        $this->resObj->data($my_data);

        return $this->resObj->__toString();
        
    } // .End doGetRedeemItems

    /**
     * Delete data banner
     */
    private function doBannerDelete() {
        // require id value
        $id = HttpParams::get("id"); // pk

        // create BannerManage object to manage
        $m = new BannerManage();

        // find data by id (pk)
        $test = $m->findById($id);

        if( count($test) > 0){ 
            $m->del($id);

            $this->resObj->statusOK();
            $this->resObj->msg("data delete success.");
            $this->resObj->data($data);
        }else{
            $this->resObj->statusFail();    
            $this->resObj->msg("data not found.");
        }
        
        return $this->resObj->__toString();

    } // .End doBannerDelete()

    //// connect soap service ////

    
    function getRefNo(){
        return chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90));
    }

    function getOTP(){
        return rand(10000,99999);
    }

    /**
     * Make send OTP to target mobile number.
     */
    private function doSendOTP($mobile){
        $sms = new thsms();
 
        $sms->username   = 'ravatna';
        $sms->password   = 'rb00ns17';
 
        $a = $sms->getCredit();
        
        if($a[0] != 1){
            $this->eventLog("Get Credit not valid OTP service.", __METHOD__);
            $this->errorLog($a[1][0], __METHOD__);
            $this->resObj->statusFail();
            $this->resObj->msg("request otp get credit fail.");
            return $this->resObj->__toString(); // .Return function here.
        } // .End if credit is false

        $ref_no = $this->getRefNo();
        $otp = $this->getOTP();
        // $mobile_no = HttpParams::get("mobile");

        $now = new DateTime();
        $created_at = $now->format("Y-m-d H:i:s"); 
        $created_by = "system"; 
        
        $updated_at = $now->format("Y-m-d H:i:s");
        $updated_by = "system";

        $status = "wait";
        $start_date = $now->format("Y-m-d H:i:s");
        $end_date = $now->format("Y-m-d H:i:s");

        $otpManage = new OtpManage();
        $cols = array( "ref_no", "otp", "start_date", "end_date", "status", "created_at", "created_by", "updated_at", "updated_by");
        $vals = array( $ref_no, $otp, $start_date, $end_date, $status, $created_at, $created_by, $updated_at, $updated_by);
        
        if($otpManage->add( $cols, $vals)){
            $b = $sms->send( '0000', $mobile, "refno:". $ref_no ." otp:" . $otp);
            if($b[0] != true){
                $this->eventLog("Call THSMS Error .", __METHOD__);
                $this->errorLog( $b[1][0], __METHOD__);
                $this->resObj->statusFail();
                $this->resObj->msg("request otp fail.");
                return $this->resObj->__toString(); // .Return function here.
            }
    
            $my_data = array();
            $dat['ref_no'] = $ref_no;
            $dat['mobile'] = $mobile;
            
            $my_data = $dat;

            $this->eventLog("Request OTP for mobile ({$mobile}) -> ref: {$ref_no} otp({$otp})", __METHOD__);

            $this->resObj->statusOK();    
            $this->resObj->msg("request otp success.");
            $this->resObj->data($my_data);
        }
        
        // return $this->resObj->__toString();

        return $my_data;
    } // .End send OTP to number mobile phone



    private function doValidOTP( ){
        $otp = HttpParams::get("otp");
        $ref_no = HttpParams::get("ref_no");
        $mobile = HttpParams::get("mobile_no");

        $otpManage = new OtpManage();
     
        $this->resObj->statusFail();    
        $this->resObj->msg("request otp fail.");

        if($otpManage->isValidOTP($ref_no,$otp)){
            $this->resObj->statusOK();    
            $this->resObj->msg("request otp success.");
            //$this->resObj->data($my_data);
        } // .End if
        
        

        return $this->resObj->__toString();
    } // .End doValidOTP()


    function ActivateCard($cardNumber,$identifierNumber,$phoneNumber,$FullName,$outletNo,$cashDeposit,$freeCash,$point,$birthday,$marryDate){
        $client = new SoapClient("http://192.168.0.34/pos/service.asmx?wsdl",
           array(
             "trace"      => 1,		// enable trace to view what is happening
             "exceptions" => 0,		// disable exceptions
             "cache_wsdl" => 0) 		// disable any caching on the wsdl, encase you alter the wsdl server
         );

       $params = array(
           'cardNumber'=>$cardNumber,
           'identifierNumber' => $identifierNumber,
           'phoneNumber' => $phoneNumber,
           'FullName' => $FullName,
           'outletNo' => $outletNo,
           'cashDeposit' => $cashDeposit,
           'freeCash' => $freeCash,
           'point' => $point,
           'birthday' => $birthday,
           'marryDate' => $marryDate
       );

       $data = $client->ActivateCard($params);

       echo "1.";
      print_r($data);
      echo "<hr />";
      echo "<br/>";
      echo "2.";
      echo $data->GetAllMemberCardListResult;
      echo "<hr />";
      echo "<br/>";

       // display what was sent to the server (the request)
       echo "<p>Request :".htmlspecialchars($client->__getLastRequest()) ."</p>";
       echo "<hr />";

       // display the response from the server
       echo "<p>Response:".htmlspecialchars($client->__getLastResponse())."</p>";

   } // .End GetCardList

    function GetAllMemberCardList($identifierNumber,$phoneNumber){
        $client = new SoapClient("http://192.168.0.34/pos/service.asmx?wsdl",
           array(
             "trace"      => 1,		// enable trace to view what is happening
             "exceptions" => 0,		// disable exceptions
             "cache_wsdl" => 0) 		// disable any caching on the wsdl, encase you alter the wsdl server
         );

       $params = array(
           'identifierNumber' => $identifierNumber,
           'phoneNumber' => $phoneNumber
       );

       $data = $client->GetAllMemberCardList($params);

      
      return $data->GetAllMemberCardListResult;

   } // .End GetCardList


    function GetCardList($identifierNumber,$phoneNumber){
 		$client = new SoapClient("http://192.168.0.34/pos/service.asmx?wsdl",
			array(
			  "trace"      => 1,		// enable trace to view what is happening
			  "exceptions" => 0,		// disable exceptions
			  "cache_wsdl" => 0) 		// disable any caching on the wsdl, encase you alter the wsdl server
		  );

        $params = array(
            'identifierNumber' => $identifierNumber,
            'phoneNumber' => $phoneNumber
        );

		$data = $client->GetCardList($params);

        // display what was sent to the server (the request)
        echo "<p>Request :".htmlspecialchars($client->__getLastRequest()) ."</p>";
        echo "<hr />";

        // display the response from the server
        echo "<p>Response:".htmlspecialchars($client->__getLastResponse())."</p>";

        echo $data->GetCardListResult;
       
    } // .End GetCardList

    function GetCardInfo($cardNumber){
        $client = new SoapClient("http://192.168.0.34/pos/service.asmx?wsdl",
        array(
            "trace"      => 1,		// enable trace to view what is happening
            "exceptions" => 0,		// disable exceptions
            "cache_wsdl" => 0) 	// disable any caching on the wsdl, encase you alter the wsdl server
        );

        $params = array(
           'cardNumber' => $cardNumber
        );

        $data = $client->GetCardInfo($params);

        // Debug::print("CardNumber: " . $data->CardInfoData->CardNumber);
        // Debug::print("IdentifierNumber: " . $data->CardInfoData->IdentifierNumber);
        // Debug::print("TelephoneNumber: " . $data->CardInfoData->TelephoneNumber);
        // Debug::print("CardType: " . $data->CardInfoData->CardType);
        // Debug::print("StartDate: " . $data->CardInfoData->StartDate);
        // Debug::print("ExpireDate: " . $data->CardInfoData->ExpireDate);
        // Debug::print("PercentDiscount: " . $data->CardInfoData->PercentDiscount);
        // Debug::print("CardValue: " . $data->CardInfoData->CardValue);
        // Debug::print("Point: " . $data->CardInfoData->Point);
        // Debug::print("CashCoupon: " . $data->CardInfoData->CashCoupon);
        // Debug::print("TitleName: " . $data->CardInfoData->TitleName);
        // Debug::print("GivenName: " . $data->CardInfoData->GivenName);
        // Debug::print("FamilyName: " . $data->CardInfoData->FamilyName);
        // Debug::print("CustomerID: " . $data->CardInfoData->CustomerID);
        // Debug::print("Birthdate: " . $data->CardInfoData->Birthdate);
        // Debug::print("Active: " . $data->CardInfoData->Active);
        // Debug::print("Result: " . $data->CardInfoData->Result);
        // Debug::print("Message: " . $data->CardInfoData->Message);
        // Debug::print("MarryDate: " . $data->CardInfoData->MarryDate);
        // Debug::print("ApplyMarryDateGift: " . $data->CardInfoData->ApplyMarryDateGift);
        // Debug::print("ApplyBirthdateGift: " . $data->CardInfoData->ApplyBirthdateGift);


        
        
        
        // if($this->IS_ENV == JIJI_DEBUG_MODE){
        //     // display what was sent to the server (the request)
        //     echo "<p>Request :".htmlspecialchars($client->__getLastRequest()) ."</p>";
        //     echo "<hr />";

        //     // display the response from the server
        //     echo "<p>Response:".htmlspecialchars($client->__getLastResponse())."</p>";
        // } // .End 

 

    return $data->CardInfoData;
   } // .End GetCardList

    /**
     * I 'm a start process
     */
    public function main(){
        switch($this->method){
            /** register member */
            case 'register_member':
                echo $this->doRegisterMember();
                break;

            /** update member info */
            case 'update_member':
                echo $this->doMemberUpdate();
                break;

            /** remove member data */
            case 'delete_member':
                echo $this->doMemberDelete();
                break;

            /** authen user */
            case 'login_authen':
                echo $this->doLoginAuthen();
                break;
            
            /** get list my card */
            case 'my_card':
                echo $this->doGetMyCard();
            break;

            /** get card list */
            case 'get_card_list':
                echo $this->doGetCardList();
                break;

                case 'get_card_list2':
                    echo $this->doGetCardList_2();
                    break;

            /** call data card info then register to our system */
            case 'get_card_info':
                echo $this->doGetCardInfo();
                break;
            
            case 'card2app':
                echo $this->doRegisterCard2App();
                break;

            case 'req_otp':            //668xxxxxxxx
                echo $this->doSendOTP();
                break;

            case 'valid_otp':
                echo $this->doValidOTP();
            break;
            /** activate new card */
            // case 'new_card':
            //     echo $this->doCardNew();
            //     break;

            case 'update_card':
                echo $this->doCardUpdate();
                break;

            case 'delete_card':
                echo $this->doCardDelete();
                break;

            /** redeem item */
            case 'get_redeem_items':
                echo $this->doGetRedeemItems();
                break;

            /** banner ads */
            case 'get_banners':
                echo $this->doGetBanners();
                break;

            case 'new_banner':
                echo $this->doBannerNew();
                break;

            case 'update_banner':
                echo $this->doBannerUpdate();
                break;

            case 'delete_banner':
                echo $this->doBannerDelete();
                break;


            case 'update_user_info':
                echo $this->doMemberUpdate();
                break;

            default:

             $expireCardNo ="1700001";
             $expireIdentityCard ="1111122222333";
             $expirePhoneNumber ="0811111111";
            
            
             $this->queryMSSQL();

            // $cardNo ="1801085";
            // $identityCard ="";
            // $phoneNumber = "";
            //$this->GetCardList($expireIdentityCard, $expirePhoneNumber);
            
            // $this->GetCardInfo($expireCardNo);

            // $cm = new CardMemberManage();
            // $cm->getCreateCommand();

        }
    } // .End main()
} // .End class

$x = new Jiji10(JIJI_PRODUCT_MODE);
$x->main();
